#!/bin/bash
sudo docker ps
echo "Logging in to docker"
aws ecr get-login-password --region us-west-2 | sudo docker login --username AWS --password-stdin 695189796512.dkr.ecr.us-west-2.amazonaws.com/login-app
echo "Fetching latest image"
sudo docker pull 695189796512.dkr.ecr.us-west-2.amazonaws.com/login-app:latest
echo "Stopping current running container"
sudo docker stop python-app
echo "Removing old container"
sudo docker rm -f python-app-old
echo "Rename stopped container"
sudo docker rename python-app python-app-old
echo "Start new container"
sudo docker run --name python-app -d -p 5000:5000 -e EMAIL=$USER_NAME -e PASSWORD=$USER_PASSWORD 695189796512.dkr.ecr.us-west-2.amazonaws.com/login-app:latest