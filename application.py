import requests, json, os
from flask import Flask

email = os.environ['EMAIL']
password = os.environ['PASSWORD']

app = Flask(__name__)

@app.route("/")
def home():
    return "Welcome, Hello World!!!"

@app.route("/login", methods=["GET"])
def login():

    api_url = "https://staging.ballogy.com/api/v3.4/auth_service/login/"

    payload = {
        'email' : email,
        'password' : password
    }
    response = requests.post(api_url, data = payload)
    return (response.json())

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True)